package cricketinfo;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Initializer {
	static WebDriver driver;

	public void initDriver() {
		System.setProperty("webdriver.chrome.driver","C:\\learn java\\cricketinfo\\src\\test\\java\\cricketinfo\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void navigateEspncricinfo() {
		driver.navigate().to("https://www.espncricinfo.com");
	}

	public void maximizeWindow() {
		driver.manage().window().maximize();
	}

	public void waitToLoad() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

}

