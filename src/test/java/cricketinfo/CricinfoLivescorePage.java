package cricketinfo;



import java.util.ArrayList;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CricinfoLivescorePage extends Initializer {
	protected List<String> url = new ArrayList<String>();
	protected ArrayList<WebElement> matches = (ArrayList<WebElement>) driver
			.findElements(By.xpath("//div[@class='match-score-block']/div[2]/a"));
	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	public void getMatchLinks() {
		// System.out.println(matches.size());
		for (int i = 1; i <= matches.size(); i++) {
			String match = ((matches.get(i - 1).getAttribute("href")));
			url.add(match);
		}
	}
	public void clickMatchLinks() {
		for (int i = 0; i < url.size(); i++) {
			driver.navigate().to(url.get(i));
			//driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='match-header-container']")));
//			 wait.until(ExpectedConditions
//						.visibilityOfElementLocated(By.xpath("//div[@class='live-scorecard']/div[2]/div/div/div/div")));
			 CricinfoMatchDetailsPage cmdp = new CricinfoMatchDetailsPage();
				cmdp.getMatchDetails();
		}
	}

}

